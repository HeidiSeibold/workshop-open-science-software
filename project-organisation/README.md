# Thoughts on digital project organisation

## Naming
Good naming conventions for files and folders (see e.g.
https://the-turing-way.netlify.app/project-design/filenaming.html) may seem
trivial, but are super helpful.

- What are good naming conventions?
- How can you use naming conventions for your research project?


## Project structure

- How do you organize your files and folders on your computer?
- R-Packages have a predefined folder structure (see e.g.
  https://swcarpentry.github.io/r-novice-inflammation/08-making-packages-R/).
  What do you think is the benefit of that?
- Can you come up with a suggestion for a project folder structure that could
  work for many research projects?

## Other
- What do you think needs to be considered in growing research teams in terms
  of project organisation?
- When and with whom should project organisation be discussed?

## If you have enough time
- Check the project folder structure and naming in one of your projects. Did
  you do a good job? What would you change now? 
