# Thoughts on documenting and licensing of research projects

## Documenting

- Who benefits from good documentation of research projects?
- What needs documenting and how does documentation differ for different
  research outputs?
- What are good documentation principles?


## Licensing

- What is a license?
- When and for what to use a license?
- Why do you think the CC-BY license is so popular in research? More on Creative Commons licenses here: https://creativecommons.org/choose/
- How can you license your project, presentation slides, etc. 


If you have enough time:
- Why are there so many different licenses in software (see e.g. https://choosealicense.com/)


