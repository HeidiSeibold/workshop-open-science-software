# First steps with version control 

## Check
- Did everyone manage to install git (see [installation
  instructions](/installation-instructions.md)? If not can you help each other
with issues? Do not spend more than 5 minutes on this part. Contact Heidi if
issues remain. If enough people have git installed, you can continue.

## Start your first git Repository

Here is one way how to do it:
- Go to GitLab and press "create new project/repository"
- Create a blank project
- Enter a name and projet description, decide if you want to intialize the
  project with a file called "README". I usually do that.
- Done :tada:

## Fiddling with git and GitLab

If you run into problems with the following, discuss with the group and help
each other. Call Heidi if you get stuck.
- Can you figure out how to clone the repository (using RStudio)?
- Can you figure out how to add new files? Do the files show up on the GitLab
  website? What do you need to do for them to show up?
- Are any additional files being generated automatically. What are they used for?
- Can you figure out how to change existing files?
- What is good practice for commit messages?
- Can you check previous changes on the GitLab website? What information can
  you get (who, when, what was done?)?

## If you have time
- Add a collaborator to your project. Can they make changes? 
- Find out how to create a branch. What is a branch good for?
- How can you work with branches in RStudio?
