# Workshop: Open Science and Software

A workshop on version control, digital project management, documentation and
licensing for digital research projects.

## Welcome

Welcome to this workshop repository :clap: :tada:

In this workshop you will learn how to:

1. Work with version control (Git, GitLab)
2. Set up a research project on your computer
3. Document and license your research project

Each topic is represented by a folder (`version-control`,
`project-organisation`, `documenting-licensing`). 

## Before the workshop

Before the workshop starts, please install R, RStudio and Git and set up a
first project on GitLab. 

:rocket: Follow these
[instructions](version-control/installation-instructions.md) :rocket:


## At the workshop

- Find a table/group to start at. Each group discusses one of the three topics (click link to see instructions):
	* [Version Control](version-control)
	* [Project Organisation](project-organisation)
	* [Documenting & Licensing](documenting-licensing)
- Round 1: 
	* Go through the discussion points, discuss, learn and note down
	   important points (50 minutes)
	* Prepare the collected information for the next group (15 minutes)
- Round 2: 1-2 people stay at the table and serve as experts for the next
  group, the rest moves on to the next table and discusses and learns about the
  next topic.
- Round 3: 1-2 people stay (not the same as before) to serve as experts for the
  next group, the rest moves on to the next table and discusses and learns
  about the next topic.
- Round 4: The people who have always moved so far stay and serve as experts
  for the next group, the rest moves on to the next table and discusses and
  learns about the next topic.
- If you have enough time at the end of round 4 (or after any other round),
  implement everything you’ve learned:
	* Add files and folders to your git repository (good names, good
	  structure, can be “fake”/empty files)
	* Add a READMDE that explains what the repository is about, add any
	  other documentation needed
	* Add a license


![](images/workshop-rounds-people.jpeg)
---

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>. See also [LICENSE](/LICENSE).

---

You enjoyed this workshop? More workshop ideas and contact:
https://heidiseibold.gitlab.io/

![](images/logo.png)
